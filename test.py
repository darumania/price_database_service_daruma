from postgresql import insert_price
import csv

def read_csv_item(csvfile):
    result = []
    with open(csvfile, newline='', encoding='utf-8') as f:
        readers = csv.reader(f)
        for row in readers:
            uid = row[0]
            name = row[1]
            catalog_code = row[2]
            key = row[3]
            product_class = row[4]
            brand_name = row[5]
            mpn = row[6]
            channel_names = row[7]
            price = row[8]
            price_before_discount = row[9]
            supplier_price = row[10]
            supplier_stock = row[11]
            supplier_name = row[12]
            inventory_cost = row[13]
            inventory_quantity = row[14]
            market_price = row[15]
            popularity_score = row[16]
            store = row[17]
            created_at = row[18]
            result.append(dict(
                uid=uid,
                name=name,
                catalog_code=catalog_code,
                key=key,
                product_class=product_class,
                brand_name=brand_name,
                mpn=mpn,
                channel_names=channel_names,
                price=price,
                price_before_discount=price_before_discount,
                supplier_price=supplier_price,
                supplier_stock=supplier_stock,
                supplier_name=supplier_name,
                inventory_cost=inventory_cost,
                inventory_quantity=inventory_quantity,
                market_price=market_price,
                popularity_score=popularity_score,
                store=store,
                created_at=created_at
            ))
    return result[1:]

def insert_all(csv_data):
    datas = read_csv_item(csv_data)
    for data in datas:
        insert_price(name=data.get('name', None),
                     brand=data.get('brand_name', None),
                     price_before_discount=data.get('price_before_discount', None),
                     price=data.get('price', None),
                     sku=data.get('sku', None),
                     supplier_code=data.get('supplier_code', None),
                     description=data.get('description', None),
                     item_url="https://www.daruma.co.id/product/%s.html" % data.get('key', None),
                     store='daruma.co.id',
                     daruma_code=None,
                     image_url=data.get('image_url', None),
                     store_url='https://www.daruma.co.id')
    print("all csv data has been inserted into price table and bigquery")

if __name__ == "__main__":
    insert_all(csv_data='daruma_store_product.csv')
