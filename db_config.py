from configparser import ConfigParser
import _constants

def config():
    if _constants.PRODUCTION == True:
        filename="database.ini"
    else:
        filename="database_test.ini"

    section = "postgresql"
    # create parser
    parser = ConfigParser()
    # read db.ini
    parser.read(filename)

    # get section
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception("Section %s not found in the %s file." % (section, filename))

    return db