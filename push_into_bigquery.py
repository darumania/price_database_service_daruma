from google.cloud import bigquery
from google.cloud.bigquery.dbapi import exceptions as bigquery_exceptions
import datetime

def push_item_to_bigquery(id,
                          name,
                          brand,
                          price_before_discount,
                          price,
                          sku,
                          supplier_code,
                          description,
                          item_url,
                          store,
                          last_update,
                          daruma_code,
                          store_url=None,
                          sales = None,
                          views = None,
                          reviews = None):
    bigquery_client = bigquery.Client()
    dataset_ref = bigquery_client.dataset('price_table_daruma_scraping_dataset')
    table_ref = dataset_ref.table('price_table_daruma_scraping_table')

    table = bigquery_client.get_table(table_ref)

    rows_to_insert = [
        (id, name, brand, price_before_discount, price, sku, supplier_code, description, item_url, store, last_update, daruma_code, store_url, sales, views, reviews)
    ]

    try:
        bigquery_client.insert_rows(table=table, rows=rows_to_insert)
    except (Exception, bigquery_exceptions) as e:
        print('ERROR: ')
        print(e)


def isEmpty(item_url):
    bigquery_client = bigquery.Client()
    dataset_id = 'price_table_daruma_scraping_dataset'
    table_id = 'price_table_daruma_scraping_table'

    query = ('SELECT * FROM `{}.{}.{}` WHERE item_url="{}" LIMIT 1'
             .format('daruma-data-dev', dataset_id, table_id, item_url))

    try:
        query_job = bigquery_client.query(query)
        if len(list(query_job.result())) > 0:
            return False
        else:
            return True
    except Exception as e:
        print("Error at isEmpty: ")
        print(e)

def update_row_bigquery(name,
                          brand,
                          price_before_discount,
                          price,
                          sku,
                          supplier_code,
                          description,
                          item_url,
                          store,
                          daruma_code,
                          store_url=None,
                          sales = None,
                          views = None,
                          reviews = None,):
    bigquery_client = bigquery.Client()
    dataset_ref = bigquery_client.dataset('price_table_daruma_scraping_dataset')
    table_ref = dataset_ref.table('price_table_daruma_scraping_table')

    table = bigquery_client.get_table(table_ref)

    rows_to_insert = [
        (name, brand, price_before_discount, price, sku, supplier_code, description, item_url, store, daruma_code, store_url, sales, views, reviews)
    ]

    query = '''
    UPDATE daruma-data-dev.price_table_daruma_scraping_dataset.price_table_daruma_scraping_table SET 
    name=%s, 
    brand=%s, 
    price_before_discount=%s,
    price=%s,
    sku=%s,
    supplier_code=%s,
    description=%s,
    store=%s,
    daruma_code=%s,
    last_update=%s,
    sales=%s,
    views=%s,
    reviews=%s 
    WHERE item_url=%s
    '''

    last_update = datetime.datetime.now()

    query = query % (name, brand, price_before_discount, price, sku, supplier_code, description, store, daruma_code, last_update, item_url, sales, views, reviews)

    try:
        bigquery_client.insert_rows(table=table, rows=rows_to_insert)
        bigquery_client.query(query)
        print('bigquery: data updated')
    except (Exception, bigquery_exceptions) as e:
        print('ERROR: ')
        print(e)

def add_new_scheme_table():
    bigquery_client = bigquery.Client()
    table_ref = bigquery_client.dataset('price_table_daruma_scraping_dataset').table('price_table_daruma_scraping_table')
    table = bigquery_client.get_table(table_ref)

    original_schema = table.schema
    new_schema = original_schema[:]
    # new_schema.append(bigquery.SchemaField('sales', 'NUMERIC'))
    # new_schema.append(bigquery.SchemaField('views', 'NUMERIC'))
    new_schema.append(bigquery.SchemaField('reviews', 'NUMERIC'))

    table.schema = new_schema
    bigquery_client.update_table(table, ['schema'])

    print("schema was added")



if __name__ == "__main__":
    add_new_scheme_table()
